import { combineReducers } from "redux";
import compare from "./compare-reducer";

const rootReducer = combineReducers({ compare });

export default rootReducer;
