// @flow
import React from 'react'
import styled from 'styled-components'
import ProductItem from './ProductItem'

type PropsT = {
  products: [
    {
      id: string,
      image: string,
      name: string,
      description: string,
      price: string
    }
  ]
}

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const ProductList = (props: PropsT) => (
  <Wrapper>
    {props.products.map((p, i) => (
      <ProductItem
        key={i}
        id={p.id}
        image={p.image}
        name={p.name}
        description={p.description}
        price={p.price}
      />
    ))}
  </Wrapper>
)

export default ProductList
