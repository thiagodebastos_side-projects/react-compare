export default [
  {
    colours: ['red', 'green', 'blue'],
    compare: false,
    condition: 'New',
    description: 'Black chair',
    id: '1',
    image: 'https://devitems.com/preview/furnish/img/product/1.jpg',
    name: 'Chair',
    price: '$39'
  },
  {
    colours: ['green', 'blue'],
    compare: true,
    condition: 'Used',
    description: 'Amazing lamp',
    id: '2',
    image: 'https://devitems.com/preview/furnish/img/product/2.jpg',
    name: 'Lamp',
    price: '$119'
  },
  {
    colours: ['red'],
    compare: false,
    condition: 'Used',
    description: 'Used Statue',
    id: '3',
    image: 'https://devitems.com/preview/furnish/img/product/3.jpg',
    name: 'Statue',
    price: '$439'
  },
  {
    colours: ['blue'],
    compare: true,
    condition: 'New',
    description: 'Large Seat',
    id: '4',
    image: 'https://devitems.com/preview/furnish/img/product/4.jpg',
    name: 'Seat',
    price: '$239'
  }
]
