// @flow
import React from 'react'
import styled from 'styled-components'

type PropsT = {
  image: string,
  name: string,
  description: string,
  price: string
}

const Wrapper = styled.div`
  flex-basis: 25%;
  border-radius: 10px;
  overflow: hidden;
  img {
    margin: 4px;
    width: 100%;
  }
`

const ProductItem = ({ image, name, description, price }: PropsT) => (
  <Wrapper>
    <img src={image} alt="" />
    <span> {name} </span> <span> {price} </span> <br />
    <span> {description} </span>
  </Wrapper>
)

export default ProductItem
