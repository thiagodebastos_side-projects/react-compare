/*
    COMPARE

    Receives an array of objects and renders them for comparison.
    Objects to be compaired have the property 'compare' set to `true`.
*/

// @flow
import React from 'react'
import styled from 'styled-components'

type PropsT = {
  products: Array<{
    name: string,
    compare: boolean,
    price: string,
    colours: Array<string>,
    condition: string
  }>
}

const Wrapper = styled.div``

const Table = styled.table`
  border: 1px solid grey;
  width: 100%;
  td {
    border: 1px solid rgba(0, 0, 0, 0.1);
  }
`

const Compare = (props: PropsT) => {
  const filteredProducts = props.products.filter(p => p.compare === true)
  return (
    <Wrapper>
      <Table>
        <thead>
          <tr>
            <th />
            {filteredProducts.map(p => (
              <th key={`header_${p.name}`}>{p.name}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Price</td>
            {filteredProducts.map(p => (
              <td key={`${p.price}_price`}>{p.price}</td>
            ))}
          </tr>
          <tr>
            <td>Colours</td>
            {filteredProducts.map(p => (
              <td key={`${p.colours}_colours`}>
                {p.colours.map(c => (
                  <span
                    key={c}
                    style={{
                      backgroundColor: `${c}`,
                      width: 20,
                      height: 20,
                      display: 'inline-block',
                      opacity: 0.5,
                      borderRadius: '50%'
                    }}
                  />
                ))}
              </td>
            ))}
          </tr>
          <tr>
            <td>Condition</td>
            {filteredProducts.map(p => (
              <td key={`${p.condition}_condition`}>{p.condition}</td>
            ))}
          </tr>
        </tbody>
      </Table>
    </Wrapper>
  )
}

export default Compare
