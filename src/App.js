import React from 'react'
import styled, { injectGlobal } from 'styled-components'
import sampleData from './sample-data'
import ProductList from './components/ProductList'
import Compare from './components/Compare'

/* eslint-disable */
/* TODO: abstract this out */
injectGlobal`
  body {
    background-color: rgb(234, 235, 236);
  }
`
/* eslint-enable */

const Wrapper = styled.div``

const Header = styled.header`
  text-align: center;
  color: #333;
  text-shadow: 3px 3px 8px rgba(0, 0, 0, 0.4);
`

const App = () => (
  <Wrapper>
    <Header>
      <h1> React Compare </h1>
    </Header>
    <ProductList products={sampleData} />
    <Compare products={sampleData} />
  </Wrapper>
)

export default App
