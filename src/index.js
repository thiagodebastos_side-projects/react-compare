/*  to implement react-hot-loader without ejecting, we must
    require("react-hot-loader/patch") as the very first line in the application
    code, before any other imports.
*/
/* eslint-disable import/first */
require('react-hot-loader/patch')

import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { configureStore /* history */ } from './redux/store/configureStore'
import App from './App'
/* eslint-enable import/first */

const store = configureStore()
delete AppContainer.prototype.unstable_handleError
render(
  <AppContainer>
    <App store={store} />
  </AppContainer>,
  document.getElementById('root')
)

//
// ─── HOT MODULE RELOADING ───────────────────────────────────────────────────
//

if (module.hot) {
  module.hot.accept('./App', () => {
    const newConfigureStore = require('./redux/store/configureStore')
    const newStore = newConfigureStore.configureStore()
    const NewApp = require('./App').default
    render(
      <AppContainer>
        <NewApp store={newStore} />
      </AppContainer>,
      document.getElementById('root')
    )
  })
}
